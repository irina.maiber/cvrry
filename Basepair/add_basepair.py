import sys
import numpy as np
import cvrry
import os

def read_chainlist(name_chainlist):
    with open(name_chainlist, 'r') as f_in:
        return [line.split() for line in f_in]

def read_pdbids_from_chainlist(name_chainlist):
    return ([case[0]+".cif" for case in read_chainlist(name_chainlist)])

def read_chain_from_chainlist(name_chainlist):
    return ([case[1] for case in read_chainlist(name_chainlist)])

def read_pdbids(name_chains):
    return ([case[0] for case in read_chainlist(name_chains)])

def base(nt):
    nt_name = nt
    for j in range(0, len(nt) - 1):
        if(nt[j] != "."):
            nt_name = nt_name.replace(nt[j], "", 1)
        else:
            break
    nt_name = nt_name.replace(".", "")
    nt_name = nt_name.replace("^", "")
    print (nt_name)
    return nt_name

def get_chain(nt):
    nt_chain = ""
    for i in range(0, len(nt) - 1):
        if(nt[i] != "."):
            nt_chain = nt_chain + nt[i]
        else:
            break
    return nt_chain
    
    
def get_basepair(coord, lines, chain):
    basepair_info = np.zeros(coord.rna_backbone.size,
                             dtype=np.int32)
    
    nucleotide_ids = cvrry.rna_coord_get_res_ids(coord.rna_backbone)
    
    nucleotide_sequence = cvrry.rna_coord_get_full_sequence(coord.rna_backbone)
    
    nucleotide_sequence_and_ids = []
    
    for x in range(0, len(nucleotide_ids)):
        nucleotide_sequence_and_ids.append(nucleotide_sequence[x] + nucleotide_ids[x])
        
    print(nucleotide_ids)
    print(nucleotide_sequence)
    print(nucleotide_sequence_and_ids)
        
    for line in lines:
        y = line. split()
        name = y[4]
        if (name == "WC" or name == "Wobble"):
            
            nt1 = y[1]; nt2 = y[2]
            chain_1 = get_chain(nt1)
            chain_2 = get_chain(nt2)
            
            if (chain_1 == chain and chain_2 == chain):
                nt1_name = base(nt1)
                nt2_name = base(nt2)
            
                index_1 = nucleotide_sequence_and_ids.index(nt1_name)
                index_2 = nucleotide_sequence_and_ids.index(nt2_name)
            
                basepair_info[index_1] = 1
                basepair_info[index_2] = -1
            
        else:
            continue
    return basepair_info
    


def main():
    chains_file = "trna.chainlist"
    pdbid = read_pdbids("trna.chainlist")
    mmcif_filename = read_pdbids_from_chainlist("trna.chainlist")
    chain_id = read_chain_from_chainlist("trna.chainlist")
    model_i = 1
    
    print(mmcif_filename)
    print(chain_id)
    
    for i in range(0, len(mmcif_filename)):
        mmcif_file_path = os.path.join("/home/stud2018/imaiber/Masterarbeit/basepair/trna", mmcif_filename[i])
        rna_frag_coord = cvrry.read_n_compute_rna_frag_coord(mmcif_file_path,
                                                         chain_id[i], model_i)
        dssr_out = mmcif_filename[i] + ".out"
        dssr_out_path = os.path.join("/home/stud2018/imaiber/Masterarbeit/basepair/trna_dssr", dssr_out)
        f = open(dssr_out_path, "r")
        lines = f.readlines()
        f.close()
        del lines[1]
        del lines[0]
    

        basepair_info = get_basepair(rna_frag_coord, lines, chain_id[i])
        
        cvrry.rna_coord_set_basepair_info(rna_frag_coord.rna_backbone,
                                      basepair_info)
        filename_out = cvrry.fn_rna_frag_coord(pdbid[i], chain_id[i], model_i)
        cvrry.rna_frag_coord_write_binary(rna_frag_coord, filename_out)

        rna_frag_coord_copy = cvrry.rna_frag_coord_read_binary(filename_out,
                                                            cvrry.YES) 
    
        sys.stdout.flush()
        cvrry.rna_frag_coord_dump(rna_frag_coord_copy)
    
        cvrry.rna_frag_coord_destroy(rna_frag_coord)
        cvrry.rna_frag_coord_destroy(rna_frag_coord_copy)
    
    
if __name__ == "__main__":
    main()            
            
            
            
            
