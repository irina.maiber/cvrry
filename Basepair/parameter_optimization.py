import sys
import numpy as np
import cvrry
import os

gap_open = 2.07671
gap_widen = 0.548336
algn_type = 0
basepair_bonus =  -0.1 


def read_chainlist(name_chainlist):
    with open(name_chainlist, 'r') as f_in:
        return [line.split() for line in f_in]

def read_pdbids_from_chainlist(name_chainlist):
    return ([case[0]+".cif" for case in read_chainlist(name_chainlist)])

def read_cvrry_name_from_chainlist(name_chainlist, chain_id):
    cvrry_filename = []
    for i in range(0, len(name_chainlist)):
        cvrry_filename += [name_chainlist[i] + "_" + chain_id[i] + "_1.cvrry"]
    return cvrry_filename

def read_pdbid_with_chain_id(name_chainlist, chain_id):
    name = []
    for i in range(0, len(name_chainlist)):
        name += [name_chainlist[i] + "_" + chain_id[i]]
    return name

def read_chain_from_chainlist(name_chainlist):
    return ([case[1] for case in read_chainlist(name_chainlist)])

def read_pdbids(name_chains):
    return ([case[0] for case in read_chainlist(name_chains)])

def get_all_pairs(pdbid):
    all_pairs = []
    for i in range(0, len(pdbid)):
        for j in range((i+1), len(pdbid)):
            if pdbid[i] < pdbid[j]:
                tupel = (pdbid[i], pdbid[j])
            else:
                tupel = (pdbid[j],pdbid[i])
            if tupel not in all_pairs:
                all_pairs.append(tupel)
    return all_pairs
                
    

def read_all_cvrry_structs_norm(path_cvrry, cvrry_filename, pdbid_and_chain_id):
    all_structs_cvrry_dic = {}
    
    norm_para = cvrry.default_normalization()
    feat_weights = cvrry.default_feat_weights()
    
    for i in range(0, len(cvrry_filename)):
        
        cvrry_file_path = os.path.join(path_cvrry, cvrry_filename[i])
        rna_frag_coord = cvrry.rna_frag_coord_read_binary(cvrry_file_path, cvrry.YES)
        
        cvrry.rna_frags_normalize(rna_frag_coord.frags, norm_para)
        cvrry.rna_frags_multiply(rna_frag_coord.frags, feat_weights)
        
        all_structs_cvrry_dic[pdbid_and_chain_id[i]] = rna_frag_coord
        
    
    cvrry.norm_params_destroy(norm_para)
    cvrry.feat_weights_destroy(feat_weights)
    
    return all_structs_cvrry_dic


def make_fill_score_matrix(rna_frag_coord_a, rna_frag_coord_b):
    seq_len_a = cvrry.rna_frag_coord_get_seq_len(rna_frag_coord_a)
    seq_len_b = cvrry.rna_frag_coord_get_seq_len(rna_frag_coord_b)
    
    score_matrix = cvrry.score_mat_new(seq_len_a, seq_len_b)
    coverage_matrix = cvrry.score_mat_new(seq_len_a, seq_len_b)
    
    cvrry.fill_matrix_cv(score_matrix, coverage_matrix, rna_frag_coord_a.frags, rna_frag_coord_b.frags)
    cvrry.scale_marginal_scores(score_matrix, coverage_matrix, rna_frag_coord_b.frags.xmer)
    cvrry.score_mat_destroy(coverage_matrix)
    
    return score_matrix


def get_all_matrices(pdbid_all_pairs, cvrry_struct_dict):
    matrix_dict = {}
    for chainid_a, chainid_b in pdbid_all_pairs:
        matrix_dict[(chainid_a, chainid_b)] = make_fill_score_matrix(
            cvrry_struct_dict[chainid_a], cvrry_struct_dict[chainid_b]
        )
    return matrix_dict

def cvrry_struct_destroy(all_structs_cvrry_dic):
    for cvrry_struct in all_structs_cvrry_dic.values():
        cvrry.rna_frag_coord_destroy(cvrry_struct)
    for struct_name in all_structs_cvrry_dic.keys():
        struct_name = None

def matrices_destroy(all_matrices):
    for score_matrix in all_matrices.values():
        cvrry.score_mat_destroy(score_matrix)
    for pair in all_matrices.keys():
        pair = None

            
def get_pcd(all_structs_cvrry_dic, all_matrices):
    
    pcd_dic = {}
    
    for pair in all_matrices.keys():
        
        pset = cvrry.score_mat_sum_minimal(all_matrices.get(pair), float(gap_open), float(gap_widen), algn_type)
        print(pset)
        pcd = cvrry.percentage_of_similar_distances_rna(pset, all_structs_cvrry_dic.get(pair[0]).rna_backbone, all_structs_cvrry_dic.get(pair[1]).rna_backbone, 5.0)
        pairs = f"{pair[0]} and {pair[1]}"
        pcd_dic[pairs] = pcd
        
    return pcd_dic
    

def main():
    chains_file = "trna.chainlist"
    pdbid = read_pdbids("trna.chainlist")
    mmcif_filename = read_pdbids_from_chainlist("trna.chainlist")
    chain_id = read_chain_from_chainlist("trna.chainlist")
    model_i = 1
    cvrry_filename = read_cvrry_name_from_chainlist(pdbid, chain_id)
    pdbid_and_chain_id = read_pdbid_with_chain_id(pdbid, chain_id)
    pdbid_all_pairs = get_all_pairs(pdbid_and_chain_id)
    path_cvrry = "/home/stud2018/imaiber/Masterarbeit/basepair/trna_cvrry"
    
    
    all_structs_cvrry_dic = read_all_cvrry_structs_norm(path_cvrry, cvrry_filename, pdbid_and_chain_id)
    all_matrices = get_all_matrices(pdbid_all_pairs, all_structs_cvrry_dic)
    
    pcd = get_pcd(all_structs_cvrry_dic, all_matrices)
    
    all_structs_cvrry_dic = cvrry_struct_destroy(all_structs_cvrry_dic)
    all_matrices = matrices_destroy(all_matrices) 
    
    #print(pcd)
    
                
if __name__ == "__main__":
    main()
